<head>
	<title>Product [index]</title>
</head>
<body>
  <p id="message">${message}</p>

	<h1>Listing Products</h1>

	<table>
		<tr>
			<th>Prueba1</th>
			<th>Prueba2</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>

		<c:forEach items="${productList}" var="product">
			<tr>
				<td>${product.prueba1}</td>
				<td>${product.prueba2}</td>
				<td><a href="${pageContext.request.contextPath}/products/${product.id}">show</a></td>
				<td><a href="${pageContext.request.contextPath}/products/${product.id}/edit">edit</a></td>
				<td>
					<form action="${pageContext.request.contextPath}/products/${product.id}" method="post">
						<input type="hidden" name="_method" value="delete"/>
						<button type="submit" onclick="return confirm('Are you sure?')">destroy</button>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>

	<br />
	<a href="${pageContext.request.contextPath}/products/new">New Product</a> 
</body>

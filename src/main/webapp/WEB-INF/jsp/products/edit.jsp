<head>
	<title>Product [edit]</title>
</head>
<body>
	<h1>Editing Product</h1>
	<form:form action="${pageContext.request.contextPath}/products/update" commandName="product">
		<form:hidden path="id"/>
		<%@include file="form.jsp"%>
		<div class="actions">
			<button type="submit">Update Product</button>
		</div>
	</form:form>
</body>

<a href="${pageContext.request.contextPath}/products">Back</a>

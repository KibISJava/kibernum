<head>
	<title>Product [new]</title>
</head>
<body>
	<h1>New Product</h1>
	<form:form action="${pageContext.request.contextPath}/products" commandName="product">
		<%@include file="form.jsp"%>
		<div class="actions">
			<button type="submit">Create Product</button>
		</div>
	</form:form>
</body>

<a href="${pageContext.request.contextPath}/products">Back</a>

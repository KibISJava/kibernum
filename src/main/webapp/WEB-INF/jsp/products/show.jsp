<head>
	<title>Product [show]</title>
</head>
<body>
  <p id="message">${message}</p>

	<p>
		<b>Prueba1:</b>
		${product.prueba1}
	</p>
	<p>
		<b>Prueba2:</b>
		${product.prueba2}
	</p>

	<a href="${pageContext.request.contextPath}/products/${product.id}/edit">Edit</a>
	<a href="${pageContext.request.contextPath}/products">Back</a>
</body>
